# Descripción

Este proyecto busca conocer las capacidades de CI/CD en los productos de Atlassian.

# Dev

Se utiliza PostgreSQL como base de datos en el puerto 5432 para development y 5431 para tests. Por defecto se usa una DB 'test' con user 'test' y password 'test', pero todo se puede configurar con las ENV variables `DATABASE_URL` y `TEST_DATABASE_URL`. Más info en `config/database.yml`.

El development se realiza como es normal en `rails s` y accesible en `localhost:3000`.

Los tests se ejecutan con `bundle exec rspec`.

### Git Flow

En Bitbucket no he encontrado la función para hacer forks. Pero en todo caso se puede trabajar sin forks pero siempre evitando hacer push a master. A grandes razgos los pasos a seguir son:

1. Clonar el repo
2. A partir de `master` crear un branch para trabajar (con un nombre que se entienda task/bug por resolver), i.e. `task-4312`
3. Trabajar localmente y asegurarse que los tests pasan (crear tests nuevos en caso de resolver un bug para evitar regresiones en un futuro)
4. Hacer push del branch por separado (asegurarse que no hay conflictos con `master`)
5. Asegurarse que los tests en el pipeline pasaron para el branch en que se trabajó. En caso de no pasar los tests volver al punto #3
6. Crear el Pull Request de `task-4312` -> `master`
