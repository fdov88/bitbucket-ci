Rails.application.routes.draw do
  devise_for :users
  root to: 'home#index'
  get '/internal', to: 'home#internal'
end
