########################
# Ploywithme Dockerfile
########################
FROM ruby:2.5
MAINTAINER Fernando Valverde <fdov88@gmail.com>

# Install yarn
RUN apt-get update && \
    apt-get install apt-transport-https && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get purge nodejs && \
    apt-get update && \
    apt-get install nodejs -y && \
    apt-get install yarn -y

# Workdir and add dependencies
WORKDIR /app/
ADD Gemfile Gemfile.lock /app/

# Throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

# Dependencies
RUN bundle install && \
    yarn install

# Add the app code and use non-root user
ADD . /app/
RUN chown -R nobody:nogroup /app
USER nobody

# Precompile assets on startup
CMD ["sh", "-c", "bin/rake assets:precompile && rails s"]
