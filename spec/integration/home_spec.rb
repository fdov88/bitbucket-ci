require 'rails_helper'

RSpec.describe "home page", :type => :request do
  it "displays the user's email after successful login" do
    user_password = "user_password"
    user = FactoryBot.create(:user, password: user_password)
    visit "/users/sign_in"
    fill_in "user_email", with: user.email
    fill_in "user_password", with: user_password
    click_button "Log in"

    expect(page).to have_content("Adiós #{user.name}!")
  end
end
