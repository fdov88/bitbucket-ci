FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "tester_#{n}@mobtion.biz" }
    sequence(:name) { |n| "tester_#{n}" }
    password { "pass123" }
  end

  # This will use the User class (Admin would have been guessed)
  # factory :admin, class: User do
  #   first_name { "Admin" }
  #   last_name { "User" }
  #   admin { true }
  # end
end
